package hu.rod0tg.asm.mock.account;

import brave.sampler.Sampler;
import feign.Logger;
import hu.rod0tg.asm.core.stockaccountservice.EnableStockAccountServices;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@EnableStockAccountServices
@SpringBootApplication
public class StockAccountMockApplication {

    public static void main(String... args){
        SpringApplication.run(StockAccountMockApplication.class,args);
    }

    @Bean
    public Sampler defaultSampler(){
        return Sampler.ALWAYS_SAMPLE;
    }

}
