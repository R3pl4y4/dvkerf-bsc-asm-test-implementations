package hu.rod0tg.asm.mock.account.adapter;

import hu.rod0tg.asm.core.stockaccountservice.domain.account.BankAccount;
import hu.rod0tg.asm.core.stockaccountservice.domain.transfer.TransferRequest;
import hu.rod0tg.asm.core.stockaccountservice.domain.transfer.TransferResponse;
import hu.rod0tg.asm.core.stockaccountservice.repository.BankAccountRepository;
import hu.rod0tg.asm.core.stockaccountservice.repository.StockAccountServiceRepository;
import hu.rod0tg.asm.core.stockaccountservice.transfer.adapter.TransactionAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class MockTransactionAdapter implements TransactionAdapter {

    @Value("${spring.application.name}")
    private String serviceId;

    @Autowired
    private StockAccountServiceRepository serviceRepository;

    @Override
    public TransferResponse initiateMoneyTransfer(TransferRequest transferRequest, BankAccount senderAccount, BankAccount recipientAccount) {

        return TransferResponse.Builder.newItem()
                .withRequest(transferRequest)
                .withMessage("Transfer finished by default. This is how mock service configured.")
                .withTransactionFee(BigDecimal.ZERO)
                .build();
    }

    @Override
    public TransferRequest.Status getTransactionInfo(TransferRequest transferRequest) {
        return TransferRequest.Status.DONE;
    }

    @Override
    public BankAccount getAccountInfo() {
        return serviceRepository.findById(serviceId).get().getBankAccount();
    }
}
