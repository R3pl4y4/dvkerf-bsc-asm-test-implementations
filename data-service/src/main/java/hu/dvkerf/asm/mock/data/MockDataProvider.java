package hu.dvkerf.asm.mock.data;

import hu.rod0tg.asm.core.stockdataservice.domain.data.StockDataRate;
import hu.rod0tg.asm.core.stockdataservice.provider.StockDataProvider;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MockDataProvider implements StockDataProvider {

    private final List<StockDataRate> dummyData = List.of(
            StockDataRate.Builder.newItem()
                    .withCompanyName("JuditkaTech")
                    .withDateOfCheck(LocalDateTime.parse("2020-12-01T11:25:25"))
                    .withOpenPrice(new BigDecimal(10))
                    .withClosePrice(new BigDecimal(12))
                    .withHighestPrice(new BigDecimal(15))
                    .withLowestPrice(new BigDecimal(9))
                    .withVolume(BigInteger.valueOf(3000))
                    .build(),
            StockDataRate.Builder.newItem()
                    .withCompanyName("JuditkaTech")
                    .withDateOfCheck(LocalDateTime.parse("2020-12-02T11:25:25"))
                    .withOpenPrice(new BigDecimal(12))
                    .withClosePrice(new BigDecimal(13))
                    .withHighestPrice(new BigDecimal(14))
                    .withLowestPrice(new BigDecimal(10))
                    .withVolume(BigInteger.valueOf(2000))
                    .build(),
            StockDataRate.Builder.newItem()
                    .withCompanyName("JuditkaTech")
                    .withDateOfCheck(LocalDateTime.parse("2020-12-03T11:25:25"))
                    .withOpenPrice(new BigDecimal(13))
                    .withClosePrice(new BigDecimal(11))
                    .withHighestPrice(new BigDecimal(13))
                    .withLowestPrice(new BigDecimal(7))
                    .withVolume(BigInteger.valueOf(3210))
                    .build()
        );


    @Override
    public List<StockDataRate> getStockRates() {
        return dummyData;
    }

    @Override
    public List<StockDataRate> getStockRates(LocalDateTime from, LocalDateTime until) {
        return dummyData.stream().filter(rate -> rate.getDateOfCheck().isAfter(from) && rate.getDateOfCheck().isBefore(until))
                .collect(Collectors.toList());
    }
}
