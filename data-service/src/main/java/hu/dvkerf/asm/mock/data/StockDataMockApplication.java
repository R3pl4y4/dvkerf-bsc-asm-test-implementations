package hu.dvkerf.asm.mock.data;

import hu.rod0tg.asm.core.stockdataservice.EnableStockDataServices;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableStockDataServices
@SpringBootApplication
public class StockDataMockApplication {

    public static void main(String... args){
        SpringApplication.run(StockDataMockApplication.class,args);
    }
}
