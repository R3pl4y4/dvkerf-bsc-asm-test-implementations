package hu.dvkerf.asm.dvkerfbscasmtrader;

import hu.rod0tg.asm.core.stocktraderservice.EnableStockTraderServices;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableStockTraderServices
@SpringBootApplication
public class DvkerfBscAsmTraderApplication {

	public static void main(String[] args) {
		SpringApplication.run(DvkerfBscAsmTraderApplication.class, args);
	}

}
