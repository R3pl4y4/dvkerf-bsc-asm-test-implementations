package hu.dvkerf.asm.dvkerfbscasmtrader;

import hu.rod0tg.asm.core.stocktraderservice.adapter.StockBrokerAdapter;
import hu.rod0tg.asm.core.stocktraderservice.domain.advice.TradingAdvice;
import hu.rod0tg.asm.core.stocktraderservice.domain.advice.TradingAdviceResponse;
import hu.rod0tg.asm.core.stocktraderservice.domain.stock.StockMarket;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BrokerAdapterMock implements StockBrokerAdapter {
    @Override
    public TradingAdviceResponse delegateStockBuyingAdvice(TradingAdvice request) {
        return null;
    }

    @Override
    public TradingAdviceResponse delegateStockSellingAdvice(TradingAdvice request) {
        return null;
    }

    @Override
    public List<StockMarket> getStockMarketInfo() {
        return new ArrayList<>();
    }
}
